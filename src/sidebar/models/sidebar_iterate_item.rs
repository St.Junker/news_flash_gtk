use crate::sidebar::FeedListItemID;
use news_flash::models::TagID;

#[derive(Clone, Debug)]
pub enum SidebarIterateItem {
    SelectAll,
    FeedListSelectFirstItem,
    FeedListSelectLastItem,
    TagListSelectFirstItem,
    TagListSelectLastItem,
    SelectFeedListItem(FeedListItemID),
    SelectTagList(TagID),
    NothingSelected,
}
